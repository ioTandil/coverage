﻿using Plugin.Geolocator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace coverage
{
    public partial class MapPage : ContentPage
    {
        public MapPage()
        {
            InitializeComponent();

            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 50;

                var position = locator.GetPositionAsync();
                if (position == null)
                    return;

                //Console.WriteLine("Position Status: {0}", position.Timestamp);
                //Console.WriteLine("Position Latitude: {0}", position.Latitude);
                //Console.WriteLine("Position Longitude: {0}", position.Longitude);
            }
            catch (Exception ex)
            {
                //Debug.WriteLine("Unable to get location, may need to increase timeout: " + ex);
            }
        }


    }

}
